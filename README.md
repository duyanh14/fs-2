VNExpress Crawler
===============
## Install

```
docker-compose -p vnec up --build
```

### Start crawl
```
docker exec -it vnec_app sh -c "go run crawl.go"
``` 

## OpenAPI

```
http://localhost:3000/swagger/index.html
```

## Test

### Login using username and password to retrieve a access token
```
curl -X POST --data "user=duyanh&password=123" http://localhost:3000/auth
```
Response
```
{
    "access_token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhZG1pbiI6dHJ1ZSwiZXhwIjoxNjYwOTc5MTcyfQ.KSh18RYnO73zYQcSu2Rw6BEU9HmWIzQqrAJhSHAz4jc"
}
```
### Get list posts
```
curl -X GET http://localhost:3000/posts?page=1&limit=10&search=viec%20lam
```

### Get post
```
curl -X GET http://localhost:3000/post/[post-id]
```

### Create post
```
curl -X POST -H "Authorization: Bearer [access-token]" -f "url=[post-url]" http://localhost:3000/post
```

### Update post
```
curl -X PUT -H "Authorization: Bearer [access-token]" -H "Content-Type: text/plain" --data-raw "[post-detail]" http://localhost:3000/post/[post-id]
```


### Delete post
```
curl -X DELETE -H "Authorization: Bearer [access-token]" http://localhost:3000/post/[post-id]
```

## Design diagram
![alt text](https://i.imgur.com/rhe1hK0.png)
