package configs

import (
	log "github.com/sirupsen/logrus"
	"github.com/spf13/viper"
)

func Get(m string) map[string]interface{} {
	config := viper.New()
	config.SetConfigName("config")
	config.AddConfigPath("./configs")
	err := config.ReadInConfig()
	if err != nil {
		log.Warn("config not found", err)
		return nil
	}
	return config.GetStringMap(m)
}
