package main

import (
	"sync"
	"time"
	"vnec/database"
	"vnec/services"

	"github.com/jasonlvhit/gocron"
	log "github.com/sirupsen/logrus"
)

func crawl() {
	log.Info("Crawl running...")

	database.Init()

	var ps = services.PostService{}
	posts := ps.GetPosts("giao-duc/tin-tuc", 20)

	for _, post := range posts {
		p := post
		go func() {
			var ps = services.PostService{
				Post: &p,
			}

			var wg sync.WaitGroup
			wg.Add(2)

			go func() {
				defer wg.Done()
				ps.GetPost()
			}()

			go func() {
				defer wg.Done()
				ps.GetPostComments()
			}()

			go func() {
				wg.Wait()
				savePost := ps.SavePost()

				log.Info("ID: ", ps.Post.ID)
				log.Info("Title: ", ps.Post.Title)
				log.Info("Description: ", ps.Post.Description)
				log.Info("Save: ", savePost)
				log.Info("======================================")
			}()
		}()

	}
}

func main() {
	// Run crawl at the first time
	crawl()

	var runEveryMinutes = 10

	gocron.Every(uint64(runEveryMinutes)).Minutes().Do(crawl)
	gocron.Start()

	for {
		time.Sleep(time.Second * 10)
	}
}
