package database

import (
	"fmt"
	"time"
	"vnec/configs"
	"vnec/models"

	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
)

var Database models.DatabaseInstance

var GetConnectionString = func(config models.DatabaseConfig) string {
	connectionString := fmt.Sprintf("%s:%s@tcp(%s)/%s?charset=utf8mb4&collation=utf8mb4_unicode_ci&parseTime=true&multiStatements=true",
		config.User,
		config.Password,
		config.Host,
		config.Database)
	return connectionString
}

func Init() {
	dbi, err := Connect()
	if err != nil {
		panic("can't connect database")
	}

	Database = *dbi

}

func Connect() (*models.DatabaseInstance, error) {
	var c = configs.Get("database")

	config := models.DatabaseConfig{
		Host:     c["host"].(string),
		Port:     c["port"].(int),
		User:     c["user"].(string),
		Password: c["password"].(string),
		Database: c["database"].(string),
	}

	dbc, err := gorm.Open(mysql.Open(GetConnectionString(config)), &gorm.Config{
		Logger: logger.Default.LogMode(logger.Silent),
	})
	if err != nil {
		panic(err)
	}

	dbc.AutoMigrate(&models.PostsTable{})

	sqlDB, err := dbc.DB()

	sqlDB.SetConnMaxLifetime(time.Minute * 5)
	sqlDB.SetMaxIdleConns(500)
	sqlDB.SetMaxOpenConns(500)

	var i = models.DatabaseInstance{
		Connect: dbc,
	}
	return &i, nil
}

func Close(dbi *models.DatabaseInstance) {
	db, _ := dbi.Connect.DB()
	db.Close()
}
