package main

import (
	"log"
	"time"
	"vnec/database"
	_ "vnec/docs"
	"vnec/middleware"
	"vnec/routes"
	"vnec/storage"

	swagger "github.com/arsmn/fiber-swagger/v2"
	"github.com/gofiber/fiber/v2"
)

// @title VNExpress Crawler API
// @version 1.0
// @host localhost:3000
// @BasePath /
// @schemes http
func main() {
	time.Sleep(8 * time.Second)

	database.Init()

	storage.Cache = storage.CacheStorage{
		DefaultExpiration: 1 * time.Hour,
		CleanInterval:     1 * time.Hour,
	}
	storage.Cache.New()

	//

	app := fiber.New()

	app.Use(middleware.CORS())

	app.Get("/", func(c *fiber.Ctx) error {
		return c.SendString("root")
	})

	app.Get("/swagger/*", swagger.HandlerDefault)

	app.Get("/posts", routes.GetPosts)
	app.Get("/post/:id", routes.GetPost)

	app.Post("/auth/login", routes.Login)

	app.Use(middleware.JWTProtected())

	app.Post("/auth", routes.Auth)

	app.Post("/post", routes.CreatePost)
	app.Put("/post/:id", routes.UpdatePost)
	app.Delete("/post/:id", routes.DeletePost)

	if err := app.Listen(":3000"); err != nil {
		log.Fatal(err)
	}
}
