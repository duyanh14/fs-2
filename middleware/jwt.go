package middleware

import (
	"vnec/configs"
	"vnec/models"

	"github.com/gofiber/fiber/v2"
	jwtware "github.com/gofiber/jwt/v3"
	"github.com/golang-jwt/jwt/v4"
)

func JWTProtected() func(*fiber.Ctx) error {
	return jwtware.New(jwtware.Config{
		SigningKey:     []byte(configs.Get("jwt")["secret"].(string)),
		SuccessHandler: jwtSuccess,
		ErrorHandler:   jwtError,
	})
}

func jwtSuccess(c *fiber.Ctx) error {
	auth := Auth(c)

	if !auth["admin"].(bool) {
		return c.JSON(models.Response{Error: models.ACCESS_DENIED})
	}

	return c.Next()
}

func jwtError(c *fiber.Ctx, err error) error {
	if err.Error() == "Missing or malformed JWT" {
		return c.Status(fiber.StatusBadRequest).
			JSON(models.Response{Error: models.ACCESS_TOKEN_MISSING, Data: "Access token is missing in the request header"})
	}
	return c.Status(fiber.StatusUnauthorized).
		JSON(models.Response{Error: models.ACCESS_TOKEN_INVALID, Data: "Access token provided is invalid or has expired"})
}

func Auth(c *fiber.Ctx) jwt.MapClaims {
	user := c.Locals("user").(*jwt.Token)
	claims := user.Claims.(jwt.MapClaims)
	return claims
}
