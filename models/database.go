package models

import "gorm.io/gorm"

type DatabaseConfig struct {
	Host     string
	Port     int
	User     string
	Password string
	Database string
}

type DatabaseInstance struct {
	Connect *gorm.DB
}
