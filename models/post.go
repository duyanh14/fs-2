package models

import (
	"regexp"
	"strconv"
)

type Post struct {
	ID          int           `json:"id"`
	URL         string        `json:"url"`
	Title       string        `json:"title,omitempty"`
	Description string        `json:"description,omitempty"`
	Thumbnail   string        `json:"thumbnail,omitempty"`
	Detail      []PostDetail  `json:"detail,omitempty"`
	Comment     []PostComment `json:"comment,omitempty"`
	Date        int64         `json:"date,omitempty"`
}

type PostsTable struct {
	ID          int `gorm:"primaryKey"`
	URL         string
	Title       string `sql:"type:CHARACTER SET utf8 COLLATE utf8_general_ci"`
	Description string `sql:"type:CHARACTER SET utf8 COLLATE utf8_general_ci"`
	Thumbnail   string
	Detail      string `sql:"type:CHARACTER SET utf8 COLLATE utf8_general_ci"`
	Comment     string `sql:"type:CHARACTER SET utf8 COLLATE utf8_general_ci"`
	Date        int64  `sql:"type:CHARACTER SET utf8 COLLATE utf8_general_ci"`
}

func (PostsTable) TableName() string {
	return "posts"
}

type PostDetail struct {
	Type        string `json:"type"`
	Value       string `json:"value"`
	Description string `json:"description,omitempty"`
}

type PostComment struct {
	ID      int              `json:"id"`
	Name    string           `json:"name"`
	Content string           `json:"content"`
	Like    int              `json:"like"`
	Date    int64            `json:"date"`
	Reply   PostCommentReply `json:"reply"`
}

type PostCommentReply struct {
	Count int                    `json:"count"`
	Item  []PostCommentReplyItem `json:"item,omitempty"`
}

type PostCommentReplyItem struct {
	ID      int    `json:"id"`
	Name    string `json:"name"`
	Content string `json:"content"`
	Like    int    `json:"like"`
	Date    int64  `json:"date"`
}

func (post *Post) GetPostID() int {
	if post.ID != 0 {
		return post.ID
	}
	var re = regexp.MustCompile(`(-)([0-9]+){1}(.html)`)
	matches := re.FindStringSubmatch(post.URL)
	postID, _ := strconv.Atoi(matches[2])
	return postID
}
