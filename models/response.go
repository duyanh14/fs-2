package models

type Response struct {
	Error int         `json:"error,omitempty"`
	Data  interface{} `json:"data,omitempty"`
}
