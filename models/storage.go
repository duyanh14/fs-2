package models

import "crypto/tls"

type RedisConfig struct {
	Host      string
	Port      int
	Username  string
	Password  string
	Database  int
	URL       string
	Reset     bool
	TLSConfig *tls.Config
}
