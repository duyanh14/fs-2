package routes

import (
	"time"
	"vnec/configs"
	"vnec/middleware"
	"vnec/models"

	"github.com/gofiber/fiber/v2"
	"github.com/golang-jwt/jwt/v4"
)

// @Summary Login account
// @Description Login account and get access token
// @ID login
// @Param user formData string true "Username"
// @Param password formData string true "Password"
// @Success 200 {object} models.Response{}
// @Router /auth/login [post]
func Login(c *fiber.Ctx) error {
	user := c.FormValue("user")
	password := c.FormValue("password")

	if user != "duyanh" || password != "123" {
		return c.JSON(models.Response{
			Error: models.LOGIN_FAILED,
		})
	}

	claims := jwt.MapClaims{
		"user":  "Duy Anh",
		"admin": true,
		"exp":   time.Now().Add(time.Hour * 24).Unix(),
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)

	t, err := token.SignedString([]byte(configs.Get("jwt")["secret"].(string)))
	if err != nil {
		return c.SendStatus(fiber.StatusInternalServerError)
	}

	return c.JSON(models.Response{Data: fiber.Map{"access_token": t}})
}

// @Summary Account authentication
// @Description Return account information
// @ID auth
// @Success 200 {object} models.Response{}
// @Router /auth [post]
func Auth(c *fiber.Ctx) error {
	return c.JSON(models.Response{Data: middleware.Auth(c)})
}
