package routes

import (
	"strconv"
	"sync"
	"time"
	"vnec/database"
	"vnec/models"
	"vnec/services"
	"vnec/storage"

	"github.com/gofiber/fiber/v2"
	_ "github.com/golang-jwt/jwt/v4"
)

// @Summary Get all posts
// @Description Get all posts, integrated pagination and keyword search
// @ID posts
// @Param page query int false "Posts from page"
// @Param limit query int false "Limit the number of posts in the page"
// @Param search query string false "Search for posts that contain keywords in title, description, and detail"
// @Success 200 {array} models.Post
// @Router /posts [get]
func GetPosts(c *fiber.Ctx) error {
	page := c.Query("page")
	limit := c.Query("limit")
	search := c.Query("search")

	type filter struct {
		page   int
		limit  int
		search string
	}

	f := filter{}

	if page != "" {
		f.page, _ = strconv.Atoi(page)
	} else {
		f.page = 1
	}

	if limit != "" {
		f.limit, _ = strconv.Atoi(limit)
	} else {
		f.limit = 10
	}

	if search != "" {
		f.search = search
	}

	data, found := storage.Cache.Get("posts_" + page + "-" + limit + "-" + search)

	if found {
		return c.JSON(models.Response{Data: data})
	}

	find := database.Database.Connect
	find = find.Select("id", "url", "title", "description", "thumbnail")

	find = find.Offset((f.page - 1) * f.limit)
	find = find.Limit(f.limit)

	find = find.Where("concat(title, description, detail) like ?", "%"+f.search+"%")

	posts := []models.PostsTable{}
	find.Find(&posts)

	response := &[]models.Post{}

	for _, p := range posts {
		post := *services.ToPost(&p)
		*response = append(*response, post)
	}

	storage.Cache.Set("posts_"+page+"-"+limit+"-"+search, *response, 1*time.Hour)

	return c.JSON(models.Response{Data: *response})
}

// @Summary Get post information
// @Description Get the full information of the post
// @ID post
// @Param id path int true "Post ID"
// @Success 200 {object} models.Post
// @Router /post/{id} [get]
func GetPost(c *fiber.Ctx) error {
	data, found := storage.Cache.Get("post_" + c.Params("id"))

	if found {
		return c.JSON(models.Response{Data: data})
	}

	post := models.PostsTable{}
	result := database.Database.Connect.Find(&post, c.Params("id"))

	if result.RowsAffected == 0 {
		return c.JSON(models.Response{Error: models.POST_NOT_FOUND})
	}

	toPost := *services.ToPost(&post)

	storage.Cache.Set("post_"+c.Params("id"), toPost, 1*time.Hour)

	return c.JSON(models.Response{Data: toPost})
}

// @Summary Import a new post
// @Description Import a new post from VNExpress through URL
// @ID create-post
// @Param url formData string true "URL of the post"
// @Success 200 {object} models.Response{}
// @Router /post [post]
func CreatePost(c *fiber.Ctx) error {
	form, err := c.MultipartForm()
	if err != nil {
		return c.JSON(models.Response{Error: models.INVALID_URL})
	}
	if len(form.Value["url"]) == 0 {
		return c.JSON(models.Response{Error: models.INVALID_URL})
	}

	post := models.Post{URL: form.Value["url"][0]}
	post.ID = post.GetPostID()

	go func() {
		ps := services.PostService{
			Post: &post,
		}

		var wg sync.WaitGroup
		wg.Add(2)

		go func() {
			defer wg.Done()
			ps.GetPost()
		}()

		go func() {
			defer wg.Done()
			ps.GetPostComments()
		}()

		wg.Wait()
		ps.SavePost()
	}()

	return c.JSON(models.Response{Data: post.ID})
}

// @Summary Update the post
// @Description Update the content of the post
// @ID update-post
// @Param content body []models.PostDetail true "New post content"
// @Success 200 {object} models.Response{}
// @Router /post [put]
func UpdatePost(c *fiber.Ctx) error {
	id, err := strconv.Atoi(c.Params("id"))

	if err != nil {
		return c.JSON(models.Response{Error: models.POST_NOT_FOUND})
	}

	// Validate new detail
	detail, errr := services.ToPostDetail(string(c.Body()))
	if errr != nil {
		return c.JSON(models.Response{Error: models.DETAIL_WRONG_FORMAT})
	}

	post := models.Post{ID: id, Detail: *detail}
	ps := services.PostService{Post: &post}

	if !ps.SavePost() {
		return c.JSON(models.Response{Error: models.SAVE_FAILED})
	}

	return c.JSON(models.Response{})
}

// @Summary Delete the post
// @Description Delete the post
// @ID delete-post
// @Param id path int true "Post ID"
// @in header
// @name Authorization
// @Success 200 {object} models.Response{}
// @Router /post/{id} [delete]
func DeletePost(c *fiber.Ctx) error {
	find := database.Database.Connect

	id, err := strconv.Atoi(c.Params("id"))
	if err != nil {
		return c.JSON(models.Response{Error: models.POST_NOT_FOUND})
	}

	result := find.Delete(models.PostsTable{}, id)

	if result.RowsAffected == 0 {
		return c.JSON(models.Response{Error: models.POST_NOT_FOUND})
	}

	return c.JSON(models.Response{})
}
