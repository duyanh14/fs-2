package services

import (
	"encoding/json"
	"errors"
	"io"
	"log"
	"net/http"
	"strconv"
	"time"
	"vnec/database"
	"vnec/models"
	"vnec/utils"

	"github.com/Jeffail/gabs"
	"github.com/PuerkitoBio/goquery"
	"github.com/anaskhan96/soup"
)

type PostService struct {
	Post *models.Post
}

func (m PostService) GetPosts(url string, params ...int) []models.Post {
	var ps []models.Post

	targetPage := 1
	currentPage := 1
	if len(params) > 0 {
		targetPage = params[0]
	}
	if len(params) > 1 {
		currentPage = params[1]
	}

	newUrl := "https://vnexpress.net/" + url
	if targetPage > 0 {
		newUrl += "-p" + strconv.Itoa(currentPage)
	}

	req, _ := http.Get(newUrl)
	req.Header.Set("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/104.0.0.0 Safari/537.36")

	body, _ := io.ReadAll(req.Body)

	doc := soup.HTMLParse(string(body))
	titles := doc.FindAll("", "class", "title-news")
	for _, title := range titles {
		postURL := title.Find("a").Attrs()["href"]
		ps = append(ps, models.Post{
			URL: postURL})
	}

	if len(ps) > 0 && targetPage > currentPage {
		return append(ps, m.GetPosts(url, targetPage, currentPage+1)...)
	}

	return ps
}

func (ps *PostService) GetPost() *models.Post {
	req, _ := http.Get(ps.Post.URL)
	req.Header.Set("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/104.0.0.0 Safari/537.36")

	doc, err := goquery.NewDocumentFromReader(req.Body)
	if err != nil {
		log.Fatal(err)
	}

	ps.Post.ID = ps.Post.GetPostID()
	ps.Post.Title = doc.Find("h1.title-detail").Text()

	doc.Find("p.description span.location-stamp").Remove()
	ps.Post.Description = doc.Find("p.description").Text()

	ps.Post.Thumbnail, _ = doc.Find("meta[itemprop=\"thumbnailUrl\"]").Attr("content")

	doc.Find("article.fck_detail").Children().Each(func(index int, selection *goquery.Selection) {
		className, classExist := selection.Attr("class")
		if !classExist {
			return
		}
		if !classExist {
			return
		}
		detail := models.PostDetail{}

		switch className {
		case "Normal":
			detail.Type = "text"
			detail.Value = selection.Text()
		case "tplCaption":
			detail.Type = "image"
			image := selection.Find("img[itemprop=\"contentUrl\"]")
			detail.Value, _ = image.Attr("data-src")
			detail.Description, _ = image.Attr("alt")
		}

		if detail.Type == "" {
			return
		}
		ps.Post.Detail = append(ps.Post.Detail, detail)
	})
	return ps.Post
}

func (ps *PostService) GetPostComments() *[]models.PostComment {
	// Get sign
	req, _ := http.Get(ps.Post.URL)
	req.Header.Set("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/104.0.0.0 Safari/537.36")

	doc, err := goquery.NewDocumentFromReader(req.Body)
	if err != nil {
		log.Fatal(err)
	}

	sign, _ := doc.Find("div#box_comment_vne").Attr("data-component-input")

	type data struct {
		Sign         string
		Article_type string
	}
	var d data
	err = json.Unmarshal([]byte(sign), &d)

	//
	req, _ = http.Get("https://usi-saas.vnexpress.net/index/get?offset=0&limit=999999999&frommobile=0&sort=like&objectid=" + strconv.Itoa(ps.Post.ID) + "&objecttype=" + d.Article_type + "&siteid=1000000&sign=" + d.Sign)
	req.Header.Set("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/104.0.0.0 Safari/537.36")
	req.Header.Set("Accept", "*/*")
	req.Header.Set("Accept-Encoding", "gzip, deflate, br")
	req.Header.Set("Connection", "keep-alive")

	doc, err = goquery.NewDocumentFromReader(req.Body)
	if err != nil {
		log.Fatal(err)
	}

	jsonParsed, err := gabs.ParseJSON([]byte(doc.Text()))
	if err != nil {
		panic(err)
	}
	a := jsonParsed.Search("data", "items")
	ac, _ := a.Children()

	for _, child := range ac {
		id := child.Path("comment_id").Data()
		name := child.Path("full_name").Data()
		content := child.Path("content").Data()
		like := child.Path("userlike").Data()
		date := child.Path("creation_time").Data()

		reply := child.Search("replys").Path("total").Data()

		if reply == nil {
			reply = 0
		} else {
			reply = int(reply.(float64))
		}

		switch v := id.(type) {
		case float64:
			id = int(v)
		case string:
			id, _ = strconv.Atoi(v)
		}

		comment := models.PostComment{
			ID:      id.(int),
			Name:    name.(string),
			Content: content.(string),
			Like:    int(like.(float64)),
			Date:    int64(date.(float64)),
			Reply: models.PostCommentReply{
				Count: reply.(int),
			},
		}

		if reply.(int) > 0 {
			comment.Reply.Item = *ps.getPostCommentReplys(&comment)
		}

		ps.Post.Comment = append(ps.Post.Comment, comment)
	}

	return &ps.Post.Comment
}

func (ps *PostService) getPostCommentReplys(postComment *models.PostComment) *[]models.PostCommentReplyItem {
	rs := []models.PostCommentReplyItem{}

	req, _ := http.Get("https://usi-saas.vnexpress.net/index/getreplay?offset=0&id=" + strconv.Itoa(postComment.ID) + "&limit=9999999")
	req.Header.Set("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/104.0.0.0 Safari/537.36")
	req.Header.Set("Accept", "*/*")
	req.Header.Set("Accept-Encoding", "gzip, deflate, br")
	req.Header.Set("Connection", "keep-alive")

	doc, err := goquery.NewDocumentFromReader(req.Body)
	if err != nil {
		log.Fatal(err)
	}
	jsonParsed, err := gabs.ParseJSON([]byte(doc.Text()))
	if err != nil {
		//panic(err)
		return &rs
	}
	a := jsonParsed.Search("data", "items")
	ac, _ := a.Children()

	for _, child := range ac {
		id := child.Path("comment_id").Data()

		switch v := id.(type) {
		case float64:
			id = int(v)
		case string:
			id, _ = strconv.Atoi(v)
		}

		name := child.Path("full_name").Data()
		content := child.Path("content").Data()
		like := child.Path("userlike").Data()
		date := child.Path("creation_time").Data()

		rs = append(rs, models.PostCommentReplyItem{
			ID:      id.(int),
			Name:    name.(string),
			Content: content.(string),
			Like:    int(like.(float64)),
			Date:    int64(date.(float64)),
		})
	}

	return &rs
}

func (ps *PostService) SavePost() bool {
	// storage.Cache.Set("post_"+fmt.Sprint(ps.Post.ID), ps.Post, 2*time.Hour)

	detail, _ := utils.JSONMarshal(ps.Post.Detail)
	comment, _ := utils.JSONMarshal(ps.Post.Comment)

	if string(detail) == "null" {
		detail = nil
	}
	if string(comment) == "null" {
		comment = nil
	}

	site := models.PostsTable{
		ID:          ps.Post.ID,
		URL:         ps.Post.URL,
		Title:       ps.Post.Title,
		Description: ps.Post.Description,
		Thumbnail:   ps.Post.Thumbnail,
		Detail:      string(detail),
		Comment:     string(comment),
		Date:        time.Now().Unix(),
	}

	var count int64
	result := database.Database.Connect.Model(&models.PostsTable{}).Where("id = ?", ps.Post.ID).Count(&count)

	if count >= 1 {
		result = database.Database.Connect.Model(&site).Updates(&site)
	} else {
		result = database.Database.Connect.Create(&site)
	}

	if result.RowsAffected > 0 {
		return true
	}
	return false
}

func ToPost(pt *models.PostsTable) *models.Post {
	detail, err := ToPostDetail(pt.Detail)
	comment := &[]models.PostComment{}

	jsonParsed, err := gabs.ParseJSON([]byte(pt.Comment))
	if err == nil {
		ac, _ := jsonParsed.Children()
		for _, child := range ac {
			replyf := child.Path("reply")

			replyCount := int(replyf.Path("count").Data().(float64))
			replyItem := &[]models.PostCommentReplyItem{}
			reply := models.PostCommentReply{
				Count: replyCount,
				Item:  *replyItem,
			}

			if replyf.Path("item") != nil {
				replyItems, _ := replyf.Path("item").Children()
				for _, reply := range replyItems {
					*replyItem = append(*replyItem, models.PostCommentReplyItem{
						ID:      int(reply.Path("id").Data().(float64)),
						Name:    reply.Path("name").Data().(string),
						Content: reply.Path("content").Data().(string),
						Like:    int(reply.Path("like").Data().(float64)),
						Date:    int64(reply.Path("date").Data().(float64)),
					})
				}
			}
			reply.Item = *replyItem

			*comment = append(*comment, models.PostComment{
				ID:      int(child.Path("id").Data().(float64)),
				Name:    child.Path("name").Data().(string),
				Content: child.Path("content").Data().(string),
				Like:    int(child.Path("like").Data().(float64)),
				Date:    int64(child.Path("date").Data().(float64)),
				Reply:   reply,
			})
		}
	}

	post := models.Post{
		ID:          pt.ID,
		URL:         pt.URL,
		Title:       pt.Title,
		Description: pt.Description,
		Thumbnail:   pt.Thumbnail,
		Detail:      *detail,
		Comment:     *comment,
		Date:        pt.Date,
	}

	return &post
}

func ToPostDetail(s string) (*[]models.PostDetail, error) {
	detail := &[]models.PostDetail{}

	jsonParsed, err := gabs.ParseJSON([]byte(s))
	if err != nil {
		return detail, errors.New("parse error")
	}
	ac, _ := jsonParsed.Children()
	for _, child := range ac {
		description, err := child.Path("description").Data().(string)
		if !err {
			description = ""
		}

		_type, err := child.Path("type").Data().(string)
		if !err {
			return detail, errors.New("wrong format")
		}

		value, err := child.Path("value").Data().(string)
		if !err {
			return detail, errors.New("wrong format")
		}

		*detail = append(*detail, models.PostDetail{
			Type:        _type,
			Value:       value,
			Description: description,
		})
	}

	return detail, nil
}
