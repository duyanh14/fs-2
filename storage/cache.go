package storage

import (
	"time"

	"github.com/patrickmn/go-cache"
)

type CacheStorage struct {
	DefaultExpiration time.Duration
	CleanInterval     time.Duration
	*cache.Cache
}

var Cache CacheStorage

func (cs *CacheStorage) New() {
	cs.Cache = cache.New(cs.DefaultExpiration, cs.CleanInterval)
}
