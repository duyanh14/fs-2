package storage

import (
	"context"
	"fmt"
	"time"
	"vnec/models"

	"github.com/go-redis/redis/v8"
)

type RedisStorage struct {
	db *redis.Client
}

var Redis *RedisStorage

func New(cfg *models.RedisConfig) *RedisStorage {
	var options *redis.Options
	var err error

	if cfg.URL != "" {
		options, err = redis.ParseURL(cfg.URL)
		options.TLSConfig = cfg.TLSConfig
		if err != nil {
			panic(err)
		}
	} else {
		options = &redis.Options{
			Addr:      fmt.Sprintf("%s:%d", cfg.Host, cfg.Port),
			DB:        cfg.Database,
			Username:  cfg.Username,
			Password:  cfg.Password,
			TLSConfig: cfg.TLSConfig,
		}
	}

	db := redis.NewClient(options)

	if err := db.Ping(context.Background()).Err(); err != nil {
		panic(err)
	}

	if cfg.Reset {
		if err := db.FlushDB(context.Background()).Err(); err != nil {
			panic(err)
		}
	}

	return &RedisStorage{
		db: db,
	}
}

func (s *RedisStorage) Get(key string) ([]byte, error) {
	if len(key) <= 0 {
		return nil, nil
	}
	val, err := s.db.Get(context.Background(), key).Bytes()
	if err == redis.Nil {
		return nil, nil
	}
	return val, err
}

func (s *RedisStorage) Set(key string, val []byte, exp time.Duration) error {
	if len(key) <= 0 || len(val) <= 0 {
		return nil
	}
	return s.db.Set(context.Background(), key, val, exp).Err()
}

func (s *RedisStorage) Delete(key string) error {
	if len(key) <= 0 {
		return nil
	}
	return s.db.Del(context.Background(), key).Err()
}

func (s *RedisStorage) Reset() error {
	return s.db.FlushDB(context.Background()).Err()
}

func (s *RedisStorage) Close() error {
	return s.db.Close()
}

func (s *RedisStorage) Conn() *redis.Client {
	return s.db
}
